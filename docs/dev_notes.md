Basic file structure example that would go in the folder lib\machine

machine.h

#ifndef MACHINE_H
#define MACHINE_H

#include <inttypes.h>
#include <task.h>

namespace VentOS {
    // Define structs and classes

}

#endif


machine.cpp

#include <machine.h>
#include <debug.h>
#include <task.h>

namespace VentOS {
    ////////// FUNCTIONS //////////
    
    // Static functions - usually helper functions

    ////////// METHODS //////////
    
    // Implement my class

}