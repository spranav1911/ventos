/*
To run this on any Arduino hardware, do something like:
  pio run -e featheresp32 -t upload
  pio device monitor
or:
  pio run -e uno -t upload
  pio device monitor

Single line run and montior:
  make pio-run-esp32
  make pio-run-uno

For native:
  make pio-run-native
*/

/* Example PIRCS commands for serial entry:
Set tidal volume to 400 ml:
{ "com": "C",  "par" : "V",  "int" : "T",  "mod" : 0,  "val" : 400  }
Set Respiration Rate to 20 bpm:
{ "com": "C",  "par" : "B",  "int" : "T",  "mod" : 0,  "val" : 200  }

 */

#ifdef ARDUINO
#include <Arduino.h>
#else // Native
#include <iostream>
#endif

#include <PIRCS.h>
#include <controller.h>
#include <debug.h>
#include <machine.h>
#include <networking.h>
#include <scheduler.h>
#include <sensor.h>
#include <serial_listen.h>
#include <serial_reporter.h>
#include <state.h>
#include <timer.h>

// This should probably be protected by build flags,
// so that we only use it in a test mode. That is,
// would switch between this and real sensor drivers.

// NOTE: This is to have a volatile_ventilator_state,
// but that should probably not be in the .h.
// Rright now there is a lot of renduncancy between that
// and the VentState in controller.cpp.
#include <controller_task_PCV.h>

using namespace VentOS;

// Each of these classes inherits from Task
// so that they can run through the scheduler
// VentController vc;
// AlarmModule am;
// DisplayController dc;
// InputController ic;
// !!!!!! THESE TASKS HAVE BEEN MOVED TO BE DEPENDENCIES OF MACHINE CONTROLLER
// !!!!!

VentilatorUI ventui;
MachineController mc;

#ifdef ENABLE_NETWORKING
NetworkingController nw;
#endif

// This code gives us a SENSOR, either mock or hardware!
#if defined(INCLUDE_VENTMON_CONFIG)
#include <vos_config_ventmon.h>
#define FOUND_CONFIG
#elif defined(INCLUDE_POLYVENT_CONFIG)
#include "SPI.h"
#include <polyvent_drive.h>
#include <vos_config_polyvent.h>
#define FOUND_CONFIG
#elif defined(INCLUDE_MOCK_CONFIG)
#include <drive_simulator.h>
#include <vos_config_mock.h>
#include <vos_simulator.h>
VosPressureSimulator vps;
#define FOUND_CONFIG
#else
#include <drive_simulator.h>
#define SENSOR_CNT 0
sensor SENSORS[0];
#endif

// This is a task
int q = 4;
SerialReporter sr;
TaskScheduler ts;

// END CUT OF CODE FROM test_sim.cpp

VentStateT vs;

#if defined(INCLUDE_POLYVENT_CONFIG)
PolyVentDrive pv_d;
Drive &d = pv_d;
#else
SimulatorDrive sim_d;
Drive &d = sim_d;
#endif

int drive_version = d.get_version();

#define CLIENT_DATA_BUFFER_SIZE 40
char drive_client_data_buff[CLIENT_DATA_BUFFER_SIZE];
// char state_client_data_buff[CLIENT_DATA_BUFFER_SIZE];

#include <vos_sim_config.h>
PressureSensorConfigT psc;
InternalTestConfiguration itc;

// This is to test if we are running out of RAM!!!

#ifdef INCLUDE_POLYVENT_CONFIG

const int cs_pins[6] = {14, 15, 16, 17, 21, 22};
TruStabilityPressureSensor sensor0(cs_pins[0], -1.50, 1.50);

void polyvent_setup(PolyVentDrive &pvd) {
  pinMode(pvd.CS_MOTORS, OUTPUT);
  digitalWrite(pvd.CS_MOTORS, HIGH);
  pinMode(pvd.CS_VALVES, OUTPUT);
  digitalWrite(pvd.CS_MOTORS, HIGH);

  for (int i = 0; i < 6; i++) {
    pinMode(cs_pins[i], OUTPUT);
    digitalWrite(cs_pins[i], HIGH);
  }

  SPI.begin();

  // This is the old setting; we currently believe this is not needed.
  // SPI.beginTransaction(SPISettings(10000, MSBFIRST, SPI_MODE0));

  DebugLnCC("BEGIN True Stability sensor Pressure:");
  //  const int cs_pins[6] = {14, 15, 16, 17, 21, 22};

  // DebugLnCC("About to call begin:");
  // sensor0.begin();
  // DebugLnCC("Done with begin");
  sensor0.begin();

  pvd.sensor0.begin(); // run sensor initialization
  delay(10);
  pvd.sensor1.begin();
  delay(10);
  pvd.sensor2.begin();
  delay(10);
  pvd.sensor3.begin();
  delay(10);
  pvd.sensor4.begin();
  delay(10);
  pvd.sensor5.begin();
  delay(10);

  if (sensor0.readSensor() == 0) {
    DebugLnCC("True Stability (plain vanilla) sensor Pressure:");
    DebugLn(sensor0.pressure());
  }
  delay(10);
  // if(pvd.sensor0.readSensor() == 0) {
  //   DebugLnCC("True Stability (PolyVent) sensor Pressure:");
  //  DebugLn( pvd.sensor0.pressure());
  // }
  // delay(10);
  DebugLnCC("SPUD");
}

#endif

void setup() {
#ifdef ARDUINO
  Serial.begin(SERIAL_BAUD_RATE);
  delay(SERIAL_DELAY);
#endif

#ifdef INCLUDE_POLYVENT_CONFIG
  polyvent_setup((PolyVentDrive &)d);
  PolyVentDrive &pvd = (PolyVentDrive &)d;
  DebugLnCC("Homing PolyVent...");
  mc.vc.settings.currently_homing = true;
  pvd.home_machine();
  delay(pvd.MAX_HOMING_TIME_MS);
#endif
  mc.vc.settings.currently_homing = false;
  mc.vc.settings.homing_mode = false;
  DebugLnCC("Resuming Operation...");

  DebugLnCC("Drive Version");
  DebugLnCC(drive_version);

  ventui.initialization();

#ifdef ARDUINO
  // This is just a serial port delay...
  delay(1500);
#endif

  DebugLnCC("---------- VentOS Starting ----------");
  /*
   * Setup the (very simple) task scheduler.
   * It is interrupt controlled with a constant time slice.
   * Tasks must promise to complete within the timeslice or
   * there will a memory error - There is no context switching!
   * There are no dynamic RTOS features like pre-emption or
   * priority scheduling so that smaller controllers such as the
   * Uno/Nano can be used, and to ensure a highly derminisitic system.
   */

  // Setting mode to 0 uses software timers the same as MAIN_V1 below
  // Setting mode to 1 uses the fixed timeslice scheduler, and you must call
  // ts.setTimeslice()
  ts.setSchedulerMode(0);

  ts.addTask(&mc);
  // !!!!!! THESE TASKS HAVE BEEN MOVED TO BE DEPENDENCIES OF MACHINE CONTROLLER
  // !!!!! Please set #define NUM_TASKS in tasks.h to match the number of tasks!

  // This task is a "reporting" task. The job here is just to report status....
  // However, this is incredibly important as a test/debug capability, even if
  // it is not actually configured for use in a non-use mode.
  // I will build this up gradually; the first thing will be just to report
  // inspiration/expiration mode, then we will add pressure and flow.
  // the inspiration/expiration really CAN be computed by the vent_controller,
  // even without proper hardware, so we will start there. Our initial
  // configuration will be to report at about 25 Hz, which is about the rate
  // of the VentMon.
  // Since every task just implements steup and run, this will be pretty simple.
  //  sr.setup();
  sr.setVentSettingsPointer(&mc.vc.settings);
  sr.setVentStatePointer(&mc.vc.state);
  ts.addTask(&sr);

#ifdef ENABLE_NETWORKING
  ts.addTask(&nw);
#endif
  ts.start();

  // Here Rob is basically set up a mock system based on Ben's Controller class
#define NUM_BREATHS_TO_TEST 10
#define DEFAULT_RR_BPM_X10 60
#define DEFAULT_E_TO_I_X10 20
#define TARGET_PRESSURE_MMH2O 50
#define MAX_PRESSURE_MMH2O 450
#define TARGET_TIDAL_VOLUME_ML 200
#define MAX_VOLUME_ML 600

  mc.setup(&ventui);

  psc.low = 50;
  psc.high = 400;
  psc.delta = 5;
  itc.pressure_cfg = &psc;

  d.client_data = drive_client_data_buff;
  d.client_data_size = CLIENT_DATA_BUFFER_SIZE;

  mc.vc.d = &d;

  // This is used for internal testing. Later we might have to pack more
  // into it, but for now we are mocking a pressure sensor with this state.
  mc.vc.state.client_data = (char *)&itc;

  set_ventilator_state(&mc.vc.state, &mc.vc.settings);

  // These use PIRDS units!
  mc.vc.settings.targetRR = DEFAULT_RR_BPM_X10;
  mc.vc.settings.targetEI = DEFAULT_E_TO_I_X10;
  mc.vc.settings.targetPressure = TARGET_PRESSURE_MMH2O;
  mc.vc.settings.targetTidalVolume = TARGET_TIDAL_VOLUME_ML;
  //  mc.vc.settings.mode = VCV;
  mc.vc.settings.mode = PCV;

  mc.vc.setup();
#ifdef ENABLE_NETWORKING
  nw.setup(&mc.vc);
#endif
}
// The four modes are are for engineers and debugging; they
// would never be used in a clinical setting.
// Emergency stop:
// { "com": "C",  "par" : "M",  "int" : "s",  "mod" : "U",  "val" : 0  }
// Continue if you are in emergency stop mode:
// { "com": "C",  "par" : "M",  "int" : "c",  "mod" : "U",  "val" : 0  }
// Execute a single breath even if you are in emergency mode:
// { "com": "C",  "par" : "M",  "int" : "1",  "mod" : "U",  "val" : 0  }
// "Home" the machine, even if in emergency stop mode.
// { "com": "C",  "par" : "M",  "int" : "h",  "mod" : "U",  "val" : 0  }
//
// { "com": "C",  "par" : "P",  "int" : "T",  "mod" : "U",  "val" : 0  }
// {"com":"C","par":"P","int":"T","mod":"B","val":400}
// {"com":"C","par":"B","int":"T","mod":"X","val":120}
// {"com":"C","par":"B","int":"T","mod":"X","val":300}
// {"com":"C","par":"I","int":"T","mod":"X","val":40}
// {"com":"C","par":"I","int":"T","mod":"X","val":10}
// {"com":"C","par":"P","int":"T","mod":"B","val":500}
// {"com":"C","par":"E","int":"T","mod":"X","val":100}

#define DEBUG_MEMORY 0
void loop() {
  // We have two tasks, and there is not reason not to
  // run them both...this may require some thought.
  ts.loop();
  ts.loop();

#ifdef ARDUINO
  delay(20);
#endif

#if DEBUG_MEMORY > 0
  DebugLnCC("freeMemory()=");
  DebugLnCC(freeMemory());
#endif
}

#ifndef ARDUINO
int main(int argc, char **argv) {
  setup();
  while (1)
    loop();
  return 0;
}
#endif
