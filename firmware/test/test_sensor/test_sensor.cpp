#include <inttypes.h>
#include <sensor.h>
#include <stdio.h>
#include <string.h>
#include <unity.h>

uint32_t pressure_sensor() {
  unsigned long p = 5;
  return p;
}
#define SENSOR_CNT 1

uint8_t sensor_cnt = SENSOR_CNT;

void test_find_sensor() {
  sensor SENSORS[SENSOR_CNT] = {
      {.type = 'D', .loc = 'I', .num = 0, .func = &pressure_sensor}};

  uint32_t len = sizeof(SENSORS) / sizeof(SENSORS[0]);
  printf("Sensor count: %d\n", len);
  TEST_ASSERT_TRUE(len == SENSOR_CNT);

  sensor_func pressure_sensor = find_sensor(SENSORS, 'D', 'I', 0);
  TEST_ASSERT_NOT_NULL(pressure_sensor);

  uint32_t x = pressure_sensor();
  printf("Pressure sensor: %u\n", x);
  TEST_ASSERT_EQUAL(5, x);
}

void process() {
  UNITY_BEGIN();
  RUN_TEST(test_find_sensor);
  UNITY_END();
}

#ifdef ARDUINO
#include <Arduino.h>
void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);
  process();
}
void loop() {
  //
}
#else
int main(int argc, char **argv) {
  process();
  return 0;
}
#endif
