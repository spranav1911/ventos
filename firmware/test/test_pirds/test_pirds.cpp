#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <unity.h>
#ifdef ARDUINO
#include <Arduino.h>
#endif
#include <PIRDS.h>

/* Create a Measurement, load it into an buffer and read it back */
void test_can_create_Measurement_and_read_back_as_byte_buffer() {
  Measurement m = {'M', 'T', 101, 'B', 3, -10115};
  const uint16_t BUFF_SIZE = 13;
  uint8_t buff[BUFF_SIZE];
  uint16_t err = fill_byte_buffer_measurement(&m, buff, BUFF_SIZE);
  TEST_ASSERT_TRUE(err == 0);
  Measurement mp = get_measurement_from_buffer(buff, BUFF_SIZE);
  TEST_ASSERT_TRUE(m.event == mp.event);
  TEST_ASSERT_TRUE(m.type == mp.type);
  TEST_ASSERT_TRUE(m.loc == mp.loc);
  TEST_ASSERT_TRUE(m.ms == mp.ms);
  TEST_ASSERT_TRUE(m.val == mp.val);
}

void test_can_create_Measurement_and_read_back_as_JSON() {
  Measurement m = {'M', 'T', 101, 'B', 3, -10115};
  const uint16_t BUFF_SIZE = 256;
  char buff[BUFF_SIZE];
  uint16_t err = fill_JSON_buffer_measurement(&m, buff, BUFF_SIZE);
  TEST_ASSERT_TRUE(err != 0);

  Measurement mp = get_measurement_from_JSON(buff, BUFF_SIZE);

  TEST_ASSERT_TRUE(m.event == mp.event);
  TEST_ASSERT_TRUE(m.type == mp.type);
  TEST_ASSERT_TRUE(m.type == mp.type);
  TEST_ASSERT_TRUE(m.loc == mp.loc);
  TEST_ASSERT_TRUE(m.ms == mp.ms);
  TEST_ASSERT_TRUE(m.val == mp.val);
}

void test_can_create_Message_and_read_back_as_byte_buffer() {
  Message m = {'E', 'M', 4000, 18, "Buckminster Fuller"};
  const uint16_t BUFF_SIZE = 18 + 7;
  uint8_t buff[BUFF_SIZE];
  uint16_t err = fill_byte_buffer_message(&m, buff, BUFF_SIZE);
  TEST_ASSERT_TRUE(err == 0);
  Message mp = get_message_from_buffer(buff, BUFF_SIZE);

  TEST_ASSERT_TRUE(m.event == mp.event);
  TEST_ASSERT_TRUE(m.type == mp.type);
  TEST_ASSERT_TRUE(m.ms == mp.ms);
  TEST_ASSERT_TRUE(m.b_size == mp.b_size);
  TEST_ASSERT_TRUE(0 == strcmp(m.buff, mp.buff));
}

void test_can_create_Message_and_read_back_as_JSON() {
  Message m = {'E', 'M', 4000, 18, "Buckminster Fuller"};
  const uint16_t BUFF_SIZE = 256 + 7;
  char buff[BUFF_SIZE];
  uint16_t err = fill_JSON_buffer_message(&m, buff, BUFF_SIZE);
  TEST_ASSERT_TRUE(err > 0);

  Message mp = get_message_from_JSON(buff, BUFF_SIZE);

  TEST_ASSERT_TRUE(m.event == mp.event);
  TEST_ASSERT_TRUE(m.type == mp.type);
  TEST_ASSERT_TRUE(m.ms == mp.ms);
  TEST_ASSERT_TRUE(m.b_size == mp.b_size);
  TEST_ASSERT_TRUE(0 == strcmp(m.buff, mp.buff));
}

void process() {
  UNITY_BEGIN();
  RUN_TEST(test_can_create_Measurement_and_read_back_as_byte_buffer);
  RUN_TEST(test_can_create_Measurement_and_read_back_as_JSON);
  RUN_TEST(test_can_create_Message_and_read_back_as_byte_buffer);
  RUN_TEST(test_can_create_Message_and_read_back_as_JSON);
  UNITY_END();
}

#ifdef ARDUINO
#include <Arduino.h>
void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);
  process();
}
void loop() {
  //
}
#else
int main(int argc, char **argv) {
  process();
  return 0;
}
#endif
