#ifdef ARDUINO
#include <Arduino.h>
#else
#include <iostream>
#endif

#include <PIRDS.h>
#include <alarm.h>
#include <controller.h>
#include <inttypes.h>
#include <sensor.h>
#include <stdio.h>
#include <string.h>
#include <unity.h>

using namespace VOS_Controller;

#ifdef INCLUDE_VENTMON_CONFIG
#include <vos_config_ventmon.h>
#define FOUND_CONFIG
#endif

#ifdef INCLUDE_MOCK_CONFIG
#include <vos_config_mock.h>
#define FOUND_CONFIG
#endif

#ifndef FOUND_CONFIG
#define SENSOR_CNT 0
sensor SENSORS[0];
#endif
// SENSORS is now defined as an array, no matter what...

#define NUM_BREATHS_TO_TEST 10
int n = 0;
int iterations = 0;
int num_breaths = 0;
long last_time = 0;
long cur_time = 0;
#define DEFAULT_RR_BPM 20.0
#define DEFAULT_E_TO_I 4.0
#define TARGET_PRESSURE_CMH2O 20.0
#define MAX_PRESSURE_CMH2O 45.0
#define TARGET_VOLUME_ML 400
#define MAX_VOLUME_ML 600

/*VentState vs;

// Possibly this should be part of the ventilator state...
sensor_func pressure_sensor;


void over_pressure_limit() {
  // this is a PIRDS pressure, so we divice by 10 to get cmH2O
  unsigned long d = pressure_sensor();
  float pressure = d/10.0;
  float OVER_PRESSURE_LIMIT = 20.0;
  TEST_ASSERT_TRUE(pressure <= OVER_PRESSURE_LIMIT);
}

void pressure_conforms_to_inspirations_a_little() {
  unsigned long d = pressure_sensor();
  float pressure = d/10.0;
  if (vs.inspiring) {
    // we should have a positive pressure...
    TEST_ASSERT_TRUE(pressure > 0.0);
  } else {
    // we should have a non-positive pressure...
    TEST_ASSERT_TRUE(pressure <= 0.0);
  }
}*/

/*#ifdef ARDUINO
void mock_println(unsigned char* buff)
{
  Serial.println(buff);
}
void mock_println(int num)
{
  Serial.println(num);
}
#else
void mock_println(char* buff)
{
  std::cout << buff << "\n";
}
void mock_println(int num)
{
  std::cout << num << "\n";
}
#endif*/

void setup() {
#ifdef ARDUINO
  Serial.begin(9600);
#endif

  set_ventilator_state(&vs);
  vs.inspiring = 1;
  vs.mode.rr_bpm = DEFAULT_RR_BPM;
  vs.mode.e_to_i = DEFAULT_E_TO_I;
  vs.mode.target_pressure_cmH2O = TARGET_PRESSURE_CMH2O;

  pressure_sensor = find_sensor(SENSORS, 'D', 'I', 0);
  TEST_ASSERT_NOT_NULL(pressure_sensor);
}

void loop() {
  if (num_breaths > NUM_BREATHS_TO_TEST) {
    UNITY_END();
    exit(0);
  }

  // wait for a second
#ifdef ARDUINO
  delay(1000);
  cur_time = millis;
#else
  cur_time += 1000;
#endif

  vs.ms_into_breath += cur_time;

  // TODO Move out of loop
  /*for (int i = 0; i < TASK_CNT; i++) {
    ventilator_state *new_vs __attribute__((unused))
      = TASKS_TO_TEST[i].run(&vs,1); // run for one ms;
    // we ignore the new ventilator state, but suppress the warning
  }*/

  UNITY_BEGIN();
  /*RUN_TEST(over_pressure_limit);
  RUN_TEST(pressure_conforms_to_inspirations_a_little);*/

  iterations++;
  num_breaths++;
  last_time = cur_time;
}

#ifndef ARDUINO
int main(int argc, char **argv) {
  setup();
  while (1)
    loop();
}
#endif
