#include <debug.h>
#include <input.h>
#include <task.h>

#define DEBUG_INPUT 0
namespace VentOS {

#ifdef ARDUINO
VentilatorUI ventui;
#endif

// This is not being properly executed!!!
bool InputController::setup(VentilatorUI *ventui) {
  this->ventui = ventui;
  return true;
}

bool InputController::run(uint32_t msNow, SetCommand *sc) {
#if DEBUG_INPUT > 0
  DebugLnCC("Input controller run");
#endif
  char buffer[256];
  if (ventui->listen(buffer, 256)) {
#if DEBUG_INPUT > 0
    DebugLnCC("read buffer\n");
    DebugLn<const char *>(buffer);
#endif
    *sc = get_set_command_from_JSON(buffer, (uint16_t)256);
  }
  return true;
}

bool InputController::connect() { return true; }

bool InputController::getMessage(Message m) { return true; }

// Get PIRCS measurement from display.
bool InputController::getMeasurement(Measurement m) { return true; }

} // namespace VentOS
