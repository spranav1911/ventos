#include "vos_config_ventmon.h"
#include <sensor.h>
#include <sensor_hw_tru_stab.h>

uint8_t sensor_cnt = SENSOR_CNT;

sensor SENSORS[SENSOR_CNT] = {
    {.type = 'D', .loc = 'I', .num = 0, .func = &sensor0},
    {.type = 'D', .loc = 'I', .num = 1, .func = &sensor1},
    {.type = 'D', .loc = 'I', .num = 2, .func = &sensor2},
    {.type = 'D', .loc = 'I', .num = 3, .func = &sensor3},
    {.type = 'D', .loc = 'I', .num = 4, .func = &sensor4},
    {.type = 'D', .loc = 'I', .num = 5, .func = &sensor5},
};

// VentState
VentStateT *vstate_for_mocking = NULL;
VentSettingsT *vsettings_for_mocking = NULL;

void set_ventilator_state(VentStateT *vstt, VentSettingsT *vset) {
  vstate_for_mocking = vstt;
  vsettings_for_mocking = vset;
}
