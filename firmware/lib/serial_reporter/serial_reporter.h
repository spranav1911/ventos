#ifndef SERIAL_REPORTER_H
#define SERIAL_REPORTER_H
#include <PIRDS.h>
#include <controller.h>
#include <inttypes.h>
#include <task.h>
#include <timer.h>

using namespace VentOS;

class SerialReporter : public Task {
private:
  VentSettingsT *vsettings;
  VentStateT *vstate;

public:
  Timer swTimer;
  SerialReporter() {
    // TODO: This code is duplicated from scheduler.h;
    // we should probably have a global utility timer
#ifdef ARDUINO
#ifndef ESP32
    swTimer = Timer(millis()); // default embedded software timer
#endif
#else
    swTimer = Timer(timeSinceEpochMs()); // native software timer
#endif
  };

  void setVentSettingsPointer(VentSettingsT *vsettings);
  void setVentStatePointer(VentStateT *vstate);

  bool setup();
  bool run(uint32_t msNow);
};

#endif
