#if defined(INCLUDE_POLYVENT_CONFIG)

// NOTE: This is highly specific to the PolyVent project
// and shoule only be included by conditional compilation.
// The PolyVent board is an ESP32 that communicates with
// Arduino Nanos that drive the stepper motors in the bellows.
// These are connected via SPI. So, basically, these
// functions are going to make SPI calls.

// Exactly what the form of the API accessed via these SPI
// calls will be is still being determined.
// One option is that it is a "raw" stepper motor interface;
// Another option is that essentially a pass through of these
// functions, and that the real work is done on the Nano.
#include <drive.h>

#include <polyvent_drive.h>
// #include <debug.h>

// POLYVENT-specific code from Nathaniel
#include <Arduino.h>
#include <HoneywellTruStabilitySPI.h>
#include <SPI.h>

#define DEBUG_LEVEL 1

void send_uint16_t(uint16_t u) {

#if defined(INCLUDE_POLYVENT_CONFIG)
  SPI.transfer(*(((uint8_t *)&u) + 0));
  //  uint8_t b1 = *(((uint8_t *)&u) + 0);

  SPI.transfer(*(((uint8_t *)&u) + 1));
  //  uint8_t b2 = *(((uint8_t *)&u) + 1);
#endif
}

namespace VentOS {

// Return the stepper motor speed in steps / second to acheive the desired flow.
uint16_t PolyVentDrive::speedForFlow(uint16_t flow_mlps) {
  // This math done by Nathaniel Bechard
  const float ml_per_step = 0.07837 * PI;
  return (uint16_t)((float)flow_mlps) /
         (ml_per_step * this->NUM_BELLOWS_IN_USE);
}

void PolyVentDrive::SettingsForSpeed(uint16_t speed, uint16_t transmission[]) {
  // These positions are intentionally high; we may not
  // achieve them. Our speed should be the limiting factor!
  uint16_t positions[2] = {this->MAX_POSITION, this->MAX_POSITION};
  uint16_t speeds[2] = {speed, speed};
  uint16_t accelerations[2] = {this->MAX_ACCELERATION, this->MAX_ACCELERATION};
  uint16_t work_or_home = this->WORK;
  uint16_t init_transmission[7] = {
      work_or_home, accelerations[0], accelerations[1], speeds[0],
      speeds[1],    positions[0],     positions[1]};
  for (int i = 0; i < 7; i++) {
    transmission[i] = init_transmission[i];
  }
}
void PolyVentDrive::moveMotor(uint16_t transmissions[7]) {
  digitalWrite(CS_MOTORS, LOW);
  for (int x = 0; x < 7; x++) {
    send_uint16_t(transmissions[x]);
  }
  digitalWrite(CS_MOTORS, HIGH);
}
void PolyVentDrive::setValves(uint8_t c1, uint8_t c2) {
  digitalWrite(CS_VALVES, LOW); // enable Slave Select
  // These bits map to the valves. "Hello world" input is all input closed,
  // output put, PIV closed. First byte controls first 8 ports Second byte
  // controls last 2 MSB = Valve 0 LSB = Valve 7 Bellows are named B0, B1 Valve0
  // = Input O2 for B0 Valve1 = Input O2 for B1 Valve2 = Input Air for B0 Valve3
  // = Input Air for B0 Valve4 = Output of B0 Valve5 = Output of B1 Valve6 =
  // Patient Inflating Valve Valve7 = not connect Port8 = Tied to fan (System
  // fan) Port9 = Tied to fan (Motor driver fan) 1 = Valve Open (energized) 0 =
  // Valve Closed

  SPI.transfer(c1);
  SPI.transfer(c2);
  digitalWrite(CS_VALVES, HIGH); // disable Slave Select
}
void PolyVentDrive::InhalationMotorSettingsForTest(uint16_t transmission[]) {
  // These
  uint16_t positions[2] = {2500, 2500};
  uint16_t speeds[2] = {800, 800};
  uint16_t accelerations[2] = {this->MAX_ACCELERATION, this->MAX_ACCELERATION};

  // send: int16 accel1, int16 accel2, int16 speed1, int16 speed2, int16
  // position_setpoint, int16 position_setpoint2, int16
  // conditions_to_ask_for_1_and_2 bit 1: enable 1, bit 2: enable 2, bit 3:
  // start_home, bit 4: is it homed?
  uint16_t work_or_home = this->WORK;
  uint16_t init_transmission[7] = {
      work_or_home, accelerations[0], accelerations[1], speeds[0],
      speeds[1],    positions[0],     positions[1]};
  for (int i = 0; i < 7; i++) {
    transmission[i] = init_transmission[i];
  }
}

void PolyVentDrive::MotorRetractionsSettingsForTest(uint16_t transmission[]) {

  uint16_t work_or_home = this->WORK;
  uint16_t positions[2] = {0, 0};
  uint16_t speeds[2] = {1000, 1000};
  uint16_t accelerations[2] = {this->MAX_ACCELERATION, this->MAX_ACCELERATION};
  // send: int16 accel1, int16 accel2, int16 speed1, int16 speed2, int16
  // position_setpoint, int16 position_setpoint2, int16
  // conditions_to_ask_for_1_and_2 bit 1: enable 1, bit 2: enable 2, bit 3:
  // start_home, bit 4: is it homed?
  uint16_t init_transmission[7] = {
      work_or_home, accelerations[0], accelerations[1], speeds[0],
      speeds[1],    positions[0],     positions[1]};
  for (int i = 0; i < 7; i++) {
    transmission[i] = init_transmission[i];
  }
}

// return number is expected flow in milliliters per second that the air drive
// thinks it can return flow_mlps is the require flow in milliliters pers second
// at_pressure_cmH2O_tenths is the pressure in the mm of water against which the
// air drive must operate duration_ms is the time that the this flow should be
// produced in ms from_now_ms is when to begin producing this flow. This can be
// used to allow the drive to prepare for action. client_data is a pointer to a
// 64 byte buffer guaranteeed to be returned on the next call undisturbed.
// custom is a pointer to an character buffer of unspecified size. It is used
// only for data specific to the given airdrive.
uint32_t PolyVentDrive::drive_flow(uint32_t flow_mlps,
                                   uint32_t at_pressure_cmH2O_tenths,
                                   uint32_t max_pressure_cmH2O_tenths,
                                   char *client_data, const char *custom) {
#if defined(INCLUDE_POLYVENT_CONFIG)

  // The PolyVent uses a bellows and a stepper motor.
  // We an model this by setting the maximim speed based on the
  // flow_mlps. We will use a relatively large position (a high tidal volume)
  // and plan not to acheive it -- we will be speed limited.
  if (!inspiration_valves_set) {
    // first, we will compute the speed for our geometry based
    // on the desired flow...
    uint16_t speed = this->speedForFlow(flow_mlps);
    if (DEBUG_LEVEL > 2) {
      DebugLnCC("SPEED");
      DebugLn(speed);
    }

    // set the valves to the allow inhalation
    uint8_t c1 = 0b00001100;
    uint8_t c2 = 0b11000000;
    this->setValves(c1, c2);

    // Now we will start the motor....

    uint16_t transmissions[7];
    this->SettingsForSpeed(speed, transmissions);
    this->moveMotor(transmissions);

    inspiration_valves_set = true;
    expiration_valves_set = false;
  }
#endif

  return flow_mlps;
}

const float CONVERT_PSI_TO_CM_H2O = 70.307;
float convert_psi_to_mm_h2o(float p_psi) {
  uint32_t measured_p = (uint32_t)p_psi * CONVERT_PSI_TO_CM_H2O * 10.0;
  return measured_p;
}

// WARNING! This is a very simple initial algorithm for testing.
// Eventually we will replace this with a PID controller.
uint16_t PolyVentDrive::compute_new_motor_speed(uint16_t current_motor_speed,
                                                uint32_t measured_p_mm_h2o,
                                                uint32_t target_p_mm_h2o) {
  uint16_t speed;
  if (measured_p_mm_h2o < target_p_mm_h2o) {
    // Here the pressure is low, so we will increase the speed,
    // up to a max speed...
    speed = current_motor_speed * 1.4;
  } else {
    // Here the pressure is high, so we will decrease the speed,
    // down to a min speed...
    speed = current_motor_speed / 1.4;
  }
  if (speed > this->MAXIMUM_MOTOR_SPEED) {
    speed = this->MAXIMUM_MOTOR_SPEED;
  }
  return speed;
}
uint32_t PolyVentDrive::drive_pressure(uint32_t flow_mlps,
                                       uint32_t at_pressure_cmH2O_tenths,
                                       uint32_t max_pressure_cmH2O_tenths,
                                       char *client_data, const char *custom) {
#ifdef INCLUDE_POLYVENT_CONFIG

  // The PolyVent uses a bellows and a stepper motor.
  // We an model this by setting the maximim speed based on the
  // flow_mlps. We will use a relatively large position (a high tidal volume)
  // and plan not to acheive it -- we will be speed limited.

  // WARNING! Calling readSensor() on these seems to hang
  // if the pressure sensor is not present. These should
  // probably be predicted with some kind of timeout, but
  // that is a low priority at present.

  // NOTE: The PolyVent has 6 ports, which could be physically
  // routed to different positions. We are assuming that
  // sensor0 (which is labeled as Port 1) ont he machine)
  // is attached to the airway
  delay(10);
  float p_psi = 1.0;
  if (sensor0.readSensor() == 0) {
    p_psi = sensor0.pressure();
    DebugLn(p_psi);
  } else {
    if (DEBUG_LEVEL > 2) {
      DebugLnCC("Pressure sensor not ready !");
      return 0;
    }
  }

  // Eventually, we will produce a PID controller
  // that uses motor speed as the control variable and pressure
  // as the input. However, for now I will implement a simpler
  // algorithm: If pressure low, increase speed, if high, decrease.

  // Convert p_psi to the PIRDS units of cmH20_tenths!
  uint32_t measured_p = (uint32_t)convert_psi_to_mm_h2o(p_psi);

  this->current_pcv_mode_speed = this->compute_new_motor_speed(
      this->current_pcv_mode_speed, measured_p, at_pressure_cmH2O_tenths);

  if (DEBUG_LEVEL > 2) {
    DebugLnCC("SPEED");
    DebugLn(this->current_pcv_mode_speed);
  }

  // set the valves to the allow inhalation
  uint8_t c1 = 0b00001100;
  uint8_t c2 = 0b11000000;
  this->setValves(c1, c2);
  inspiration_valves_set = true;
  expiration_valves_set = false;

  // Now we will start the motor....

  uint16_t transmissions[7];
  this->SettingsForSpeed(this->current_pcv_mode_speed, transmissions);
  this->moveMotor(transmissions);

  inspiration_valves_set = true;
  expiration_valves_set = false;
#endif

  ((uint32_t *)client_data)[0] = at_pressure_cmH2O_tenths;
  // Being a simulation, this is a "perfect" drive...
  return at_pressure_cmH2O_tenths;
}

// NOTE: In the case of PolyVent, pause_and_prep may be
// especially important, because the bellows have to return to
// a "home" position before the next breath begins, which takes
// some time. Additionally to this, this phase is used by the bellows
// to do oxygen mixing, a critical feature of PolyVent.
uint32_t PolyVentDrive::pause_and_prep(uint32_t timer_ms, char *client_data,
                                       const char *custom) {
  //    Debug<const char*>("driver pausing and preping\n");

  // Send the data over SPI
  // ...
  // ...
  //  Debug<const char*>("pause_and_prep called: ");
  //  Debug<uint32_t>(timer_ms);
  // Just to be sneaky, I will add the volume produced since the last
  // pause_and_prep call to my client_data!
#if defined(INCLUDE_POLYVENT_CONFIG)

  if (!expiration_valves_set) {
    // set valves to allow exhalation
    uint8_t c1 = 0b00110010;
    uint8_t c2 = 0b11000000;
    this->setValves(c1, c2);

    // retract motor...
    uint16_t transmissions[7];
    this->MotorRetractionsSettingsForTest(transmissions);
    this->moveMotor(transmissions);

    inspiration_valves_set = false;
    expiration_valves_set = true;
  }

#endif

  return timer_ms;
}

void PolyVentDrive::home_machine() {
  if (DEBUG_LEVEL > 0) {
    DebugLnCC("HOMING MACHINE");
  }
  uint16_t work_or_home = this->HOME;
  uint16_t home_transmissions[7] = {work_or_home, 0, 0, 0, 0, 0, 0};
  this->moveMotor(home_transmissions);
}

uint32_t PolyVentDrive::get_version() { return POLYVENT_VERSION; }

} // namespace VentOS
#endif
