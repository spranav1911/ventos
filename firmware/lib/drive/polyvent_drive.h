#ifndef POLYVENT_DRIVE_H
#define POLYVENT_DRIVE_H

#include <debug.h>
#include <drive.h>
#include <inttypes.h>

#if defined(INCLUDE_POLYVENT_CONFIG)
#include <HoneywellTruStabilitySPI.h>

namespace VentOS {
class PolyVentDrive : public Drive {

private:
  void moveMotor(uint16_t transmissions[6]);
  void setValves(uint8_t c1, uint8_t c2);
  void InhalationMotorSettingsForTest(uint16_t transmission[]);
  void MotorRetractionsSettingsForTest(uint16_t transmission[]);
  uint16_t speedForFlow(uint16_t flow_mlps);
  void SettingsForSpeed(uint16_t speed, uint16_t transmission[]);
  uint16_t compute_new_motor_speed(uint16_t current_motor_speed,
                                   uint32_t measured_p_mm_h2o,
                                   uint32_t target_p_mm_h2o);

public:
  const uint32_t MAX_HOMING_TIME_MS = 15000;

  const uint8_t cs_pressure_pins[6] = {14, 15, 16, 17, 21, 22};

  TruStabilityPressureSensor sensor0{cs_pressure_pins[0], -1.50, 1.50};
  TruStabilityPressureSensor sensor1{cs_pressure_pins[1], -1.50, 1.50};
  TruStabilityPressureSensor sensor2{cs_pressure_pins[2], -1.50, 1.50};
  TruStabilityPressureSensor sensor3{cs_pressure_pins[3], -1.50, 1.50};
  TruStabilityPressureSensor sensor4{cs_pressure_pins[4], -1.50, 1.50};
  TruStabilityPressureSensor sensor5{cs_pressure_pins[5], -1.50, 1.50};

  const int CS_MOTORS = 4;
  const int CS_VALVES = 2;
  const int NUM_BELLOWS_IN_USE = 2;

  const uint16_t WORK = 0x0;
  const uint16_t HOME = 0xFFFF;

  const uint16_t MAX_ACCELERATION = 4000;
  const uint16_t MAX_POSITION = 5000;

  // Note: This is just a guess based on the compliance of a
  // particular test lung. This is only a starting point.

  const uint16_t MAXIMUM_MOTOR_SPEED = 1000;
  const uint16_t BASE_SPEED_PER_CM_H2O = 800;
  uint16_t current_pcv_mode_speed = BASE_SPEED_PER_CM_H2O;

  boolean inspiration_valves_set = false;
  boolean expiration_valves_set = false;

  // uncomment below for constant values
  uint8_t conditionals = 0;

  // bit 1: enable 1, bit 2: enable 2, bit 3: start_home, bit 4: is it homed?
  //    uint16_t transmission[7] = {work_or_home_word,accelerations[0],
  //    accelerations[1], speeds[0], speeds[1], positions[0], positions[1], 0};
  // bitwrite for conditions in transmission[6]

  // return number is expected flow in milliliters per second that the air drive
  // thinks it can return flow_mlps is the require flow in milliliters pers
  // second at_pressure_cmH2O_tenths is the pressure in the mm of water against
  // which the air drive must operate duration_ms is the time that the this flow
  // should be produced in ms from_now_ms is when to begin producing this flow.
  // This can be used to allow the drive to prepare for action. client_data is a
  // pointer to a 64 byte buffer guaranteeed to be returned on the next call
  // undisturbed. custom is a pointer to an character buffer of unspecified
  // size. It is used only for data specific to the given airdrive.
  uint32_t drive_flow(uint32_t flow_mlps, uint32_t at_pressure_cmH2O_tenths,
                      uint32_t max_pressure_cmH2O_tenths, char *client_data,
                      const char *custom);

  uint32_t drive_pressure(uint32_t flow_mlps, uint32_t at_pressure_cmH2O_tenths,
                          uint32_t max_pressure_cmH2O__tenths,
                          char *client_data, const char *custom);

  // NOTE: In the case of PolyVent, pause_and_prep may be
  // especially important, because the bellows have to return to
  // a "home" position before the next breath begins, which takes
  // some time. Additionally to this, this phase is used by the bellows
  // to do oxygen mixing, a critical feature of PolyVent.
  uint32_t pause_and_prep(uint32_t timer_ms, char *client_data,
                          const char *custom);
  // Home machine
  void home_machine();
  uint32_t get_version();
};
} // namespace VentOS

#endif
#endif
