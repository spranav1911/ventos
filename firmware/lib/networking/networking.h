#ifndef NETWORKING_H
#define NETWORKING_H

#include <PIRCS.h>
#include <PIRDS.h>
#include <controller.h>
#include <server.h>
#include <task.h>

// using VentOS_Networking;

namespace VentOS {

class NetworkingController : public Task {
public:
  bool setup(VentController *vc);
  bool run(uint32_t msNow);
  bool connect();
  bool send_udp(const void *data, size_t s);
};

} // namespace VentOS

#endif