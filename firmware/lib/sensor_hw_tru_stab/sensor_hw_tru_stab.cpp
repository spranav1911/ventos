#include <Arduino.h>
// #include <unity.h>
#include <debug.h>
#include <sensor_hw_tru_stab.h>

uint32_t sensor_gen(int num) {
  if (sensor0.readSensor() == 0) {
    return sensor0.pressure();
  } else {
    // we need a well defined sentinel for this!
    return 0xffffffff;
  }
}

uint32_t sensor0() { return sensor_gen(0); }
uint32_t sensor1() { return sensor_gen(1); }
uint32_t sensor2() { return sensor_gen(2); }
uint32_t sensor3() { return sensor_gen(3); }
uint32_t sensor4() { return sensor_gen(4); }
uint32_t sensor5() { return sensor_gen(5); }

uint32_t differential_sensor() {
  uint32_t p = (uint32_t)readHSCPressure();
  return p;
}

void local_setup() {}
