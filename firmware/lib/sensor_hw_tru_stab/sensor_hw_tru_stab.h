#ifndef SENSOR_HW_TRU_STAB_H
#define SENSOR_HW_TRU_STAB_H

#include <sensor.h>

/* typedef unsigned long (*sensor_func)(); */

/* struct sensor { */
/*     char type; */
/*     char loc; */
/*     uint8_t  num; */
/*     sensor_func func; */
/* }; */

uint32_t differential_sensor();

uint32_t sensor0();
uint32_t sensor1();
uint32_t sensor2();
uint32_t sensor3();
uint32_t sensor4();
uint32_t sensor5();

void local_setup();

#endif
