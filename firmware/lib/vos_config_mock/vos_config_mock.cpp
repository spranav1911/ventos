#include <controller.h>
#include <debug.h>
#include <sensor.h>
#include <vos_config_mock.h>
#include <vos_sim_config.h>

// TODO: Some of this code is now needed in the ventmon build
// state, if only for good "mock" testing. It is currently
// duplicated; but this should be moved into a shared
// file somehow, or moved on the controller or something
// so that there is little code duplication.

using namespace VentOS;

// unsigned long simulatedPressureReading(bool);
// TODO: Any mock sensors must be a function of the ventilator state.
// We probably therefore need an initialization function which establishes
// a hidden local for us to look at.

uint8_t sensor_cnt = SENSOR_CNT;

// VentState
VentStateT *vstate_for_mocking = NULL;
VentSettingsT *vsettings_for_mocking = NULL;

void set_ventilator_state(VentStateT *vstt, VentSettingsT *vset) {
  vstate_for_mocking = vstt;
  vsettings_for_mocking = vset;
}

// Note: this returns a PIRDS pressure (10ths of cmH2O)
uint32_t diff_pressure_sensor() {

#if 1 // could use a compiler flag, say VOS_USE_SIMULATED_PRESSURE_SENSOR
      // need to discuss/decide aappropriately

  if (vstate_for_mocking && vsettings_for_mocking) {
    return simulatedMockPressureReading(vstate_for_mocking,
                                        vsettings_for_mocking);
  } else
    // #else
    return (vstate_for_mocking->ventFlow == INSPIRING
                ? vsettings_for_mocking->targetPressure
                : 0);
#endif
}

// Modify/rename/remove this method appropriately based on design
// unsigned long use_abnormal_pressure_reading() {
//   if (vstate_for_mocking &&
//         vsettings_for_mocking) {
//     return
//     simulatedMockPressureReading(vstate_for_mocking,vsettings_for_mocking);
//   }
//   else
// // #else
//   return (vstate_for_mocking->ventFlow == INSPIRING ?
//           vsettings_for_mocking->targetPressure :
//           0);
// }

// Note: Mocking a flow sensor requires some sort of compliance model...
// we will add that later
uint32_t flow_sensor() {

#if 1 // could use a compiler flag, say VOS_USE_SIMULATED_PRESSURE_SENSOR
      // need to discuss/decide aappropriately

  if (vstate_for_mocking && vsettings_for_mocking) {
    return simulatedMockFlowReading(vstate_for_mocking, vsettings_for_mocking);
  } else
    // #else

    ms_in_inspiration_controller(vsettings_for_mocking->targetPressure,
                                 vsettings_for_mocking->targetPressure);
  // This path is not really what we want to use,
  // but we can use targetVolume/inspiration time to get a flow.
  return (vstate_for_mocking->ventFlow == INSPIRING ? 311 : 0);
#endif
}

sensor SENSORS[SENSOR_CNT] = {
    {.type = 'D', .loc = 'I', .num = 0, .func = &diff_pressure_sensor},
    {.type = 'F', .loc = 'I', .num = 0, .func = &flow_sensor}};
