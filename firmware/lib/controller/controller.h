#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <alarm.h>
#include <drive.h>
#include <inttypes.h>
#include <task.h>
// #include <controller_task_PCV.h>
#include <PIRCS.h>

using namespace VentOS_Alarm;

namespace VentOS {

enum WaveformShape {
  SQUARE,
  SINE,
  TAPERED
  // etc...
};

enum VentFlow { OFF, INSPIRING, EXHALING, ERROR_NO_FLOW, ERROR_OTHER };

enum VentMode {
  PCV, // Pressure control ventilation
  VCV, // Volume control ventilation
  PSV, // Pressure support ventilation
  PRVC // Pressure regulated volume control
  // etc...
};

enum VentStatus {
  NotReady,
  ReadyToSetup,
  ReadyToRun,
  Running,
  Paused,
  Stopped
};

// TODO: Add typdefs here for reuse
// TODO: Change all of these to use "Hungarian notation" for units
// These are set by the clinician
typedef struct VentSettings {
  // PatientData patientData;
  bool emergency_stop_mode = false;
  bool homing_mode = false;
  bool currently_homing = false; // tracks if we are currently homing,
  // in order to prevent all other actions during this time.
  bool breath_counting_mode = false;
  uint16_t num_breaths_to_perform = 0;
  VentMode mode = PCV;
  WaveformShape waveform = SQUARE;
  uint16_t targetRR = 0;       // Breaths per minute
  uint16_t targetEI = 0;       // EI Ratio
  uint16_t targetPressure = 0; // Pi  - should be in 1/10ths cmH20 (mmH20)
  uint16_t maxPressure = 0;    // mmHH20
  //    uint16_t targetVolume = 0;
  uint16_t maxVolume = 0;
  uint16_t targetFlow = 0;
  uint16_t targetTidalVolume = 0;
  uint16_t targetPEEP = 0; // mmHH20
  uint16_t targetFiO2 = 0;
  uint16_t triggerFlow = 0;     // insp
  uint16_t triggerPressure = 0; // insp
} VentSettingsT;

// This is the real-time state of the ventilator
// Copied from Erich's list (TODO: go back and auto gen this?)
// TODO: some of these are calculated values, some direct sensor valves
// and some is patient data which all need to be separated
// Depending on what sensors are available, some modes wont be able to be used
typedef struct VentState {
  VentStatus ventStatus = NotReady;
  VentFlow ventFlow = OFF;
  uint16_t breathTime = 0; // ms
  uint16_t inspTime = 0;   // ms
  uint16_t expTime = 0;
  uint16_t inspPressure = 0;
  uint16_t peakPressure = 0;
  uint16_t inspTidalVolume = 0;
  uint16_t expTidalVolume = 0;
  uint16_t minuteVolume = 0; // respRate * tidalVolume
  uint16_t circuitPressure = 0;
  uint16_t respRate = 0;
  uint16_t pressureSensor1 = 0;
  uint16_t pressureSensor2 = 0;
  uint16_t inspFlow = 0;
  uint16_t expFlow = 0;
  uint16_t flowSmoothed = 0;
  uint16_t patientFO2 = 0;
  uint16_t patientPCO2 = 0;
  uint16_t ventFiO2 = 0;
  uint16_t ventFeO2 = 0;
  uint16_t ventEtCO2 = 0;
  uint16_t ventICO2 = 0;
  uint16_t airwayTemperature = 0;
  uint16_t oxygenInletPressure = 0; // at the back of the machine
  uint16_t airInletPressure = 0;    // at the back of the machine
  uint16_t internalTemperature = 0; // location?
  // PartStatus batteryStatus = GOOD;
  // PartStatus mainsPowerStatus = GOOD;

  // This is a hack Rob is doing to get this to compile
  uint32_t ms_time_of_begin_of_current_breath_cycle = 0;
  char *client_data;
} VentStateT;

uint32_t ms_in_inspiration_controller(uint32_t rr, uint32_t e_to_i_x10);

class VentController : public Task {

public:
  VentController() {
    //
  }

  // These need to public for reporting...
  // there may be a more disciplined way of doing this, but
  // we are after all on embedded system with limited memory
  VentState state;       // public for dev only
  VentSettings settings; // public for dev only
  Drive *d;

  bool setup();
  bool run(uint32_t msNow);

  bool setupPCVMode(uint16_t rr, uint16_t pi, uint16_t eiRatio, uint16_t peep,
                    WaveformShape waveform);
  bool setupVCVMode(uint16_t rr, uint16_t vi, uint16_t eiRatio, uint16_t peep,
                    WaveformShape waveform);

  // setupPSVMode...
  // setupPRVCMode...
  bool runPCVMode(uint32_t msNow);
  bool runVCVMode(uint32_t msNow);

  AlarmEvent runVentilation(uint32_t msNow);
  bool pauseVentilation();
  bool stopVentilation();

  VentState setState(VentState state);
  VentState getState();

  // This takes a PIRCS command and attempts to change
  // the setttings in response to it. Returns true
  // if it is able to interpret PIRCS command,
  // false if unable.
  // Note there are some issues about exactly WHEN
  // such a change should take effect which this is not
  // addressing. Likewise, the machine should probably
  // acknowledge the command is successfully processed
  // to whomever sent the command, but we currently have
  // now way to do that.
  bool respondToControlCommand(SetCommand *sc);
  bool setCommand(SetCommand sc);

  //    uint32_t ms_in_inspiration_controller(uint32_t rr,uint32_t e_to_i_x10);
};

} // namespace VentOS

#endif

// These 2 structs are experimental
/*  struct Reading {
    uint16_t target;
    uint16_t actual;
    uint16_t min;
    uint16_t max;
  };

  struct MySettings {
    Reading rRate;
    Reading eiRatio;
    Reading pInsp;
    Reading vInsp;
    Reading fInsp;
    Reading pExh;
    Reading vExh;
    Reading fExh;
    Reading vTidal;
    Reading pPEEP;
    Reading fiO2;
    Reading eCO2;
  };
*/

// if (setting.rRate > max) { trigged alarm }

/*struct VentState  {
  bool inspiring; // 1 = true, inspiration, 0 = false, exhalation
  uint16_t ms_into_breath;
  pcv_mode mode;
};
*/

/*
  struct pcv_mode {
    float rr_bpm;
    float e_to_i;
    float target_pressure_cmH2O;
  };

  typedef VentState* (*task_run_ms)(VentState *v,unsigned t);

  struct task {
      task_run_ms run;
  };

  // Run Pressure Controlled Ventilation for t milliseconds
  // Take a state and return a state
  VentState* run_pcv(VentState *v, unsigned t);

  #define TASK_CNT 1

  extern task TASKS[TASK_CNT];

  void controller_task_init();

*/
