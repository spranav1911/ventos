#include <alarm.h>
#include <controller.h>
#include <controller_task_PCV.h>
#include <debug.h>
#include <task.h>

#if defined(INCLUDE_POLYVENT_CONFIG)
#include <polyvent_drive.h>
#else
#include <drive_simulator.h>
#endif

#include <localization.h>
#include <task.h>

using namespace VentOS_Alarm;

namespace VentOS {

#define DEBUG_CONTROLLER 2

////////// FUNCTIONS //////////

void CalculateWaveform(WaveformShape waveform) {}

////////// METHODS //////////

/*Drive dr;
  bool r = false;
  char* cd;
  char *custom;

  uint32_t tLast = 0;

  bool inspiring = true;
  uint32_t msStart = 0;

  uint32_t tPeriod = 2000; //ms
  uint32_t flowInsp = 10;
  uint32_t flowExh = 0;
  uint32_t pressureInsp = 400;
  uint32_t pressureExh = 50;
*/

bool VentController::setup() {
  DebugLnCC("Vent controller setup");
  return true;
}

bool VentController::run(uint32_t msNow) {
#if DEBUG_CONTROLLER > 1
  DebugLnCC("Vent controller run");
#endif
  // runVentilation(msNow);
  return true;
}

AlarmEvent VentController::runVentilation(uint32_t msNow) {
#if DEBUG_CONTROLLER > 2
  DebugLnCC("runVentilation");
#endif

  // So the first thing that we want to do to make
  // sure that we are in a "clinical" state.
  // if we are in an emergency of "engieering" state,
  // our action is a bit different.

  // The first state is "emergency_stop". If we are
  // in a emergency_stop and there is no overriding
  // condition, then we just return immediately.
  // if we are in breath_counting_mode, however,
  // we will run until the "num_breaths_to_perform" is zero.
  // This counter should be decremented when we exit the
  // exhalation phase.
  if (settings.emergency_stop_mode && !settings.homing_mode &&
      !((settings.breath_counting_mode &&
         settings.num_breaths_to_perform > 0))) {
#if DEBUG_CONTROLLER > 2
    DebugLnCC("emergency stop mode");
#endif
    return None;
  }
  if (settings.currently_homing) {
#if DEBUG_CONTROLLER > 2
    DebugLnCC("emergency stop mode");
#endif
    return None;
  }

  if (settings.homing_mode && !settings.currently_homing) {
#ifdef INCLUDE_POLYVENT_CONFIG
    DebugLnCC("Homing PolyVent...");
    settings.currently_homing = true;
    PolyVentDrive *pvd = (PolyVentDrive *)d;
    pvd->home_machine();
    delay(pvd->MAX_HOMING_TIME_MS);
#endif

    DebugLnCC("testing homing");

    settings.currently_homing = false;
    settings.homing_mode = false;
    DebugLnCC("Resuming Operation...");
  }
  // Run algorithms here
  switch (settings.mode) {
  case PCV:
    if (!runPCVMode(msNow)) {
      DebugLnCC("ERROR: Critical error running PCV ventilation!");
      return VentCriticalPCV;
    }
    break;
  case VCV:
    if (!runVCVMode(msNow)) {
      DebugLnCC("ERROR: Critical error running VCV ventilation!");
      return VentCriticalPCV;
    }
    break;
  case PSV:
    DebugLnCC("PSV MODE NOT IMPLEMENTED");
    break;
  case PRVC:
    DebugLnCC("PRVC MODE NOT IMPLEMENTED");

    break;
  default:
    DebugLnCC("Ventilation mode invalid!");
    return VentInvalid;
    break;
  }

  return None;
}

VentState VentController::setState(VentState state) {
  return this->state = state;
}

bool VentController::setupPCVMode(uint16_t rr, uint16_t pi, uint16_t eiRatio,
                                  uint16_t peep, WaveformShape waveform) {

  settings.mode = PCV;
  settings.targetRR = rr;
  settings.targetPressure = pi;
  settings.targetEI = eiRatio;
  settings.targetPEEP = peep;
  settings.waveform = waveform;

  // TODO: Calculate waveform values
  //
  //       |``````|          |``..
  //       |      |          |    ``|
  //  _____|      |    ______|      |

  return true;
}

bool VentController::setupVCVMode(uint16_t rr, uint16_t vi, uint16_t eiRatio,
                                  uint16_t peep, WaveformShape waveform) {

  settings.mode = VCV;
  settings.targetRR = rr;
  settings.targetTidalVolume = vi;
  settings.targetEI = eiRatio;
  settings.targetPEEP = peep;
  settings.waveform = waveform;

  // TODO: Calculate waveform values
  //
  //       |``````|          |``..
  //       |      |          |    ``|
  //  _____|      |    ______|      |

  return true;
}

// Note: These are in the PIRDS units, bpm = rr / 10.0;
uint32_t ms_in_inspiration_controller(uint32_t rr_x10, uint32_t e_to_i_x10) {
  float breath_length_s = 60.0 / (rr_x10 / 10.0);
  return (uint32_t)1000.0 * breath_length_s *
         (1.0 / (((float)e_to_i_x10 / 10.0) + 1.0));
}
bool VentController::runPCVMode(uint32_t msNow) {
#if DEBUG_CONTROLLER > 2
  DebugLnCC("Running PCV Ventilation");
#endif
  uint32_t ms_insp =
      ms_in_inspiration_controller(settings.targetRR, settings.targetEI);
  // targetRR is tenths BPM
  uint32_t ms_total_breath = 60000 / (settings.targetRR / 10.0);
  uint32_t current_ms_into_breath =
      msNow - state.ms_time_of_begin_of_current_breath_cycle;

#if DEBUG_CONTROLLER > 2
  DebugLn(ms_insp);
  DebugLn(ms_total_breath);
  DebugLn(current_ms_into_breath);
  DebugLn(settings.targetEI);
#endif

  if (current_ms_into_breath < 0) {
    // This represents some sort of internal error.
    return false;
  }

  // Here we are actutally calling the the function that
  // will move the air_drive.  I would like to remove the vvs
  // from this to unify with Ben's code.
  const uint32_t ms_to_run = 10;
  run_experimental_pcv(&state, &settings, d, ms_to_run);

  if ((current_ms_into_breath > ms_insp) && (state.ventFlow == INSPIRING)) {
    // transition to EXPIRATION
    state.ventFlow = EXHALING;
  } else if ((current_ms_into_breath > ms_total_breath) &&
             (state.ventFlow == EXHALING)) {
    // These are an atom!
    state.ventFlow = INSPIRING;
    state.ms_time_of_begin_of_current_breath_cycle = msNow;
    if (settings.breath_counting_mode) {
      settings.num_breaths_to_perform--;
    }
  }

  return true;
}

bool VentController::runVCVMode(uint32_t msNow) {
#if DEBUG_CONTROLLER > 2
  DebugLnCC("Running VCV Ventilation");
#endif
  uint32_t ms_insp =
      ms_in_inspiration_controller(settings.targetRR, settings.targetEI);
  // targetRR is tenths BPM
  uint32_t ms_total_breath = 60000 / (settings.targetRR / 10.0);
  uint32_t current_ms_into_breath =
      msNow - state.ms_time_of_begin_of_current_breath_cycle;

#if DEBUG_CONTROLLER > 2
  DebugLn(ms_insp);
  DebugLn(ms_total_breath);
  DebugLn(current_ms_into_breath);
  DebugLn(settings.targetEI);
#endif

  if (current_ms_into_breath < 0) {
    // This represents some sort of internal error.
    return false;
  }

  // Here we are actutally calling the the function that
  // will move the air_drive.  I would like to remove the vvs
  // from this to unify with Ben's code.
  const uint32_t ms_to_run = 10;
  run_experimental_vcv(&state, &settings, d, ms_to_run);

  if ((current_ms_into_breath > ms_insp) && (state.ventFlow == INSPIRING)) {
    // transition to EXPIRATION
    state.ventFlow = EXHALING;
  } else if ((current_ms_into_breath > ms_total_breath) &&
             (state.ventFlow == EXHALING)) {
    // These are an atom!
    state.ventFlow = INSPIRING;
    state.ms_time_of_begin_of_current_breath_cycle = msNow;
    if (settings.breath_counting_mode) {
      settings.num_breaths_to_perform--;
    }
  }
  return true;
}

bool VentController::respondToControlCommand(SetCommand *sc) {
  // In some ways this could be considered an "interpretation" of PIRCS...
  // We're going to deconstruct the set command and change

  // Right now, a set command is the only thing we now how to process!
  if (sc->command != 'C')
    return false;

  int32_t v = sc->val;
  switch (sc->parameter) {
  case 'M': {
    char m = sc->interpretation;
#if DEBUG_CONTROLLER > 0
    DebugLnCC("Mode set: ");
    DebugLn(m);
#endif

    // These are the engineering settings...
    switch (m) {
    case 's':
      settings.emergency_stop_mode = true;
      break;
    case 'c':
      settings.emergency_stop_mode = false;
      settings.breath_counting_mode = false;
      break;
    case '1':
      settings.breath_counting_mode = true;
      settings.num_breaths_to_perform = 1;
      settings.emergency_stop_mode = true;
      break;
    case 'h':
      settings.homing_mode = true;
      settings.emergency_stop_mode = true;
      break;
      // These are the clinical modes;
    case 'V':
      settings.mode = VCV;
      break;
    case 'P':
      settings.mode = PCV;
      break;
    default:
      DebugLnCC("Unsupported ventialtion mode");
      break;
    }
    break;
  }

  case 'P': { // PIP units are mm H2O pressure
    // not sure how to do range checking on this yet...
    settings.targetPressure = v;
#if DEBUG_CONTROLLER > 0
    DebugLnCC("targetPressure set");
#endif
  } break;
  case 'V': {
    settings.targetTidalVolume = v;
#if DEBUG_CONTROLLER > 0
    DebugLnCC("targetVolume set");
#endif
  } break;
  case 'B': { // Units Breaths per minute
    settings.targetRR = v;
#if DEBUG_CONTROLLER > 0
    DebugLnCC("targetRR set");
#endif
  } break;
  case 'I': { // E:I ration times 10
    settings.targetEI = v;
#if DEBUG_CONTROLLER > 0
    DebugLnCC("targetEI");
#endif
  } break;
  case 'E': { // PEEP units are mmH2O pressure
    settings.targetPEEP = v;
#if DEBUG_CONTROLLER > 0
    DebugLnCC("targetPeep");
#endif
  } break;
  default: // if we don't recognized the parameter, we are doomed
    return false;
    break;
  }
#if DEBUG_CONTROLLER > 0
  DebugLn<int32_t>(v);
#endif
  return true;
}

// This method gets commands pushed to it, which are then processed
bool VentController::setCommand(SetCommand sc) {
  if (sc.command != '\0') {
    char buff[96];
    bool processed = this->respondToControlCommand(&sc);
    Acknowledgement ack =
        processed ? get_success_ack_from_command(&sc)
                  : get_error_ack_from_command(&sc, 'E',
                                               999); // 999 = MAGIC_ERROR_NUMBER
    uint16_t len = fill_JSON_buffer_with_ack(&ack, buff, 80);
    return true;
  }
  return false;
}

} // namespace VentOS
