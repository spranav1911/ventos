// This file is highly specific to the PolyVent
// Configuration
#include "vos_config_polyvent.h"
#include <sensor.h>
#include <sensor_honeywell.h>

uint8_t sensor_cnt = SENSOR_CNT;

// Note: It is not clear the really is a Honeywell
// sensor on the PolyVent; I need to get the specs from
// Nathaniel Bechard. --- rlr
// sensor SENSORS[SENSOR_CNT] = { { .type = 'D',
//                         .loc = 'I',
//                         .num = 0,
//                         .func = &differential_sensor }};

sensor SENSORS[0];

// VentState
VentStateT *vstate_for_mocking = NULL;
VentSettingsT *vsettings_for_mocking = NULL;

void set_ventilator_state(VentStateT *vstt, VentSettingsT *vset) {
  vstate_for_mocking = vstt;
  vsettings_for_mocking = vset;
}
