#ifndef SENSOR_H
#define SENSOR_H

#include <inttypes.h>

typedef uint32_t (*sensor_func)();

typedef struct sensor {
  uint8_t type;
  uint8_t loc;
  uint8_t num;
  sensor_func func;
} Sensor;

extern uint8_t sensor_cnt;

sensor_func find_sensor(sensor ss[], uint8_t type, uint8_t loc, uint8_t num);

#endif
