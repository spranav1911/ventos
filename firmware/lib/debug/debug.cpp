#include <debug.h>

// PIO will not build without this placeholder file!

// This is a way to measure free memory!!!
#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char *sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif // __arm__

int freeMemory() {
  char top;

  // TODO: This is probably no perfect; documentation opaque. -rlr
#ifdef ESP_PLATFORM
  return ESP.getFreeHeap();
#elif defined ARDUINO
#ifdef __arm__
  return &top - reinterpret_cast<char *>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif // __arm__
#else
  return -1;
#endif
}
