#include <PIRCS.h>
#include <alarm.h>
#include <controller.h>
#include <debug.h>
#include <display.h>
#include <input.h>
#include <machine.h>
#include <machine_config.h>
#include <task.h>

#ifdef ARDUINO
#include <Arduino.h>
#else // Native
#include <iostream>
#endif

using namespace VentOS_Alarm;

#define DEBUG_MACHINE 2
namespace VentOS {

// TODO: These should be members on the class, I think - rlr
//    VentController vc;
AlarmModule am;
DisplayController dc;
InputController ic;

////////// METHODS //////////

bool MachineController::setup(VentilatorUI *ventui) {
  DebugLnCC("Machine controller setup");
  stage = Standby;
  this->vc.setup();
  am.setup();
  dc.setup();
  DebugLnCC("About to do VentUI setup!");
  ic.setup(ventui);
  DebugLnCC("Machine controller setup DONE!");
  return true;
}

bool MachineController::run(uint32_t msNow) {
#if DEBUG_MACHINE > 2
  DebugLn<const char *>("Machine controller run, stage: ");
  DebugLn<int>(stage);
#endif

  switch (stage) {
  case Standby: {
    if (bootMachine() == MachineSuccess) {
      stage = Starting;
    } else {
      stage = Standby;
    }
    break;
  }
  case Starting: {
    if (startMachine() == MachineSuccess) {
      stage = Ready;
    } else {
      stage = Starting;
    }
    break;
  }

  case Ready: {
    stage = Setup;
    break;
  }
  case Setup: {
    // Setup the ventilator here
    if (setupMachine() == MachineSuccess) {
      stage = MachineRunning;

      // NOTE: It is very important to start the Ventilator Inspiraiton in the
      // Controller at this point...
      vc.state.ventStatus = Running;
      // TODO: These go together atomically,
      // so should be done with a single function!
      vc.state.ventFlow = INSPIRING;
      vc.state.ms_time_of_begin_of_current_breath_cycle = msNow;
    } else {
      stage = Setup;
    }
    break;
  }
  case MachineRunning: {
    SetCommand sc;
    sc.command = '\0';
    bool ic_rv = ic.run(msNow, &sc);
    if (!ic_rv) {
      DebugLnCC("ERROR: Input critical failure!");
    } else {
      // now we must do something with your SetCommand
      if (sc.command != '\0') {
        if (DEBUG_MACHINE >= 1) {
          DebugLnCC("Responding to command val:");
          DebugLn(sc.command);
          DebugLn(sc.interpretation);
          DebugLn(sc.val);
        }
        // Here we basically want to process the SetCommand (which is in the
        // PIRCS format) to change the Machine Settings..
        bool processed = vc.respondToControlCommand(&sc);
        char buff[96];
#define MAGIC_ERROR_NUMBER 999
        Acknowledgement ack =
            (processed)
                ? get_success_ack_from_command(&sc)
                : get_error_ack_from_command(&sc, 'E', MAGIC_ERROR_NUMBER);

        uint16_t len = fill_JSON_buffer_with_ack(&ack, buff, 80);
        if (len >= 80) {
          DebugLnCC("WARNING: BUFFER OVERFLOW IN machine.cpp");
        }
      }
    }

    // Run ventilation mode and trigger alarm if needed
    AlarmEvent ae = vc.runVentilation(msNow);
    if (ae != None) {
      am.trigger(ae, msNow, 60);
    }

    if (!am.run(msNow)) {
      DebugLnCC("ERROR: Alarm critical failure!");
    }

    if (!dc.run(msNow)) {
      DebugLnCC("ERROR: Display critical failure!");
    }
    break;
  }
  case Disabled: {
    DebugLnCC("Machine disabled!");
    break;
  }
  case ShuttingDown: {
    DebugLnCC("Machine shutting down!");
    break;
  }
  default:
    DebugLnCC("ERROR: Machine critical failure! Invalid status!");
    break;
  }

  return true;
}

// Very first function that is called when the ventilator is powered on
MachineResult MachineController::bootMachine() {
  DebugLn<const char *>("bootMachine");

  // Get machine config
  // Validate machine config

  // Run startup diagnostics
  // .....

  return MachineSuccess;
}

MachineResult MachineController::setupMachine() {
  DebugLnCC("setupMachine");
  // Wait for settings to be inputted

#ifndef ARDUINO

  uint16_t in[5];

  std::cout << ("Ventilator mode (0 = PCV, 1 = VCV): ");
  std::cin >> in[0];
  VentMode m = static_cast<VentMode>(in[0]);

  std::cout << ("Respiration rate (1-10): ");
  std::cin >> in[1];

  if (m == PCV) {
    std::cout << ("Target pressure (10-50cmH20): ");
    std::cin >> in[2];
  } else {
    std::cout << ("Target volume (200-800ml): ");
    std::cin >> in[2];
  }

  std::cout << ("EI ratio (1-4): ");
  std::cin >> in[3];

  std::cout << ("PEEP (0-15cmH20): ");
  std::cin >> in[4];

  if (m == PCV) {
    vc.setupPCVMode(in[1], in[2], in[3], in[4], SQUARE);
  } else if (m == VCV) {
    vc.setupVCVMode(in[1], in[2], in[3], in[4], SQUARE);
  } else {
    DebugCC("Mode selection is invalid!");
    return MachineFailed;
  }

#else
  // Uno/ESP32 serial input here...
  // TODO: Ling

#endif

  return MachineSuccess;
}

MachineResult MachineController::startMachine() {
  DebugLnCC("startMachine");
  // Read config file and set pins etc.

  return MachineSuccess;
}

} // namespace VentOS
